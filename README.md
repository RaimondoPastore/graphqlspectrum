# GraphQl Spectrum

## Instruction

- Open the folder
- Run the command ```npm install```
- Now you can run the GraphQl Server with the command ```node src/server.js```
- Open the html file in the path ```view/view.html```

## Query

There are 3 endpoints and the relative queries :

- /areas  
``` 
Get all the areas.
Query => query {
                areas {
                    id name frequencies
                }
        }
```
- /allSpectrum
``` 
Get all the spectrum.
Query => query {
                allSpectrum {
                    x y
                }
        }
```
- /spectrum(areaId : id)
``` 
Get the spectrum of an area with area.id == id.
Query => query {
                spectrum(areaId: id) {
                    x y
                }
        }
