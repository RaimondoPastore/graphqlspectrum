# Programming Assignment


-   What would you consider the most important frameworks/libraries you used? What are they used for and why did you choose them?
``` 
I think the most interesting tecnology that I have used is GraphQl. 
I have used it to realize the server endpoints.
I have chosen it because I have heard about it but never used it, so for curiosity :)
```
-   Did you learn something new while working on this assignment?
``` 
Basically I have used as much new tecnology as possible. I have used for the first time:
- Node.js
- GraphQl and graphql-yoga
- React.js
```

-   Which parts are you most proud of or did you focus on? And why?
``` 
I have spent more time on GraphQl server because GraphQl seems like a really interesting tecnology
```
-   How many hours did you spend on the assignment? How would you improve your solution if you had more time?
``` 
I have used 2 full days (all the weekend) because I have pushed myself to try new tecnolgies.
So I spent the most of the time to choose the rigth frameworks and tecnologies, then I tried to realize the prototype.
If I had used more time I could have tried to use Typescript. I tried to use it but I was not  able to configure Node.js in the proper way. 
I was also not able to create a Node.js module, for this reason I left the classes for the persistence in server.js file
I could have made a better design of the application and add the documentation.
```
-   How did you find the test overall? If you have any suggestions on how we can improve the test, we'd love to hear
    them :)
``` 
It was really interesting and gave me the chance to try new tecnologies. 
I had some problem to decide how to realize the front-end but I have always this kind of problem. 
I usually ask a lot of question to be sure that I have undestood well the specifications. 
For example I realized the endpoint `/spectrum/{:area}` but I was not sure how to use it.
```

