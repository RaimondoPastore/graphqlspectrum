class Element extends React.Component {
    render() {
        return (
            <button onClick={() => clickElement(this.props.area)}>
        {this.props.area.name}
    </button>);
    }
}

class ListElement extends React.Component {
    render() {
        let list = this.props.areas.map((a,i) => <Element area={a} key={i}/>)
        return (
            <div>{list}</div>
            );
    }
}

function renderReactElement(areas, placeholder) {
    ReactDOM.render(
    <ListElement areas={areas}/>,
    document.getElementById(placeholder)
);
}

