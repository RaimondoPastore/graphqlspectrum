var chart
var url = 'http://localhost:4000/'

getSpectrum()
getAreas()


function getAreas(){

    let query = {query: "{areas {id,name,frequencies}}"}
    let successCallback = (data) => {
        renderReactElement(data.data.areas, "listElementId")
        document.getElementById("labelId").style.display = "block";
    }
    queryToServer(url, query, successCallback)
}

function getSpectrum() {

    let query = {query: "{allSpectrum{x y}}"}
    let successCallback = (data) => {
        chart = initChart(chart, data.data.allSpectrum, "chartContainer")
    }

    queryToServer(url, query, successCallback)
}

function queryToServer(url, query, successCallback){

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify(query)
    })
        .then(r => r.json())
        .then(data => successCallback(data))
        .catch((err)=> alert("Error. Check if the GraphQL Server is running."));
}

function initChart(chart, data, chartId) {

    chart = new CanvasJS.Chart(chartId, {
        animationEnabled: true,
        zoomEnabled: true,
        title:{
            text: "Spectrum"
        },
        axisX: {
            title:"Frequency",
        },
        axisY:{
            title: "Intensity",
        },
        data: [{
            type: "line",
            markerType: "none",  //"circle", "square", "cross", "none"
            markerSize: 10,
            toolTipContent: "<b>frequency:</b> {x}<br><b>intensity:</b> {y}",
            dataPoints: data
        }]
    });
    chart.render();

    return chart
}

function updateChartByArea(area){

    let datasets = chart.options.data[0].dataPoints

    let freq = area.frequencies
    if(Array.isArray(freq) && freq.length == 2){
        let start = freq[0]
        let end = freq[1]

        datasets.forEach(d => {
            if(d.x >= start && d.x <= end){
                d.lineColor = "red"
            } else {
                delete d.lineColor
            }
        })

        chart.render();
    }

}

function clickElement(area){
    if(area) {
        updateChartByArea(area)
        setText("spanId", area.name)
    }
}

function setText(spanId, text){
    let span = document.getElementById(spanId)
    if(span) span.textContent = text;
}