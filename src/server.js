const { GraphQLServer } = require('graphql-yoga')

//*******************************************************************
// Persistence Classes
//*******************************************************************

//*******************************************************************
// AreaDAO represents a simple Dao for the access to data in
// data/areas.json
//*******************************************************************
class AreaDAO {

  constructor(path) {
    this.areas = require(path) || []
  }

  getAreas(){
    return this.areas
  }

  getArea(areaId){
    let area
    let areas = this.getAreas()
    if(areas) area = areas.find(a => a.id == areaId)
    return area ? area : null
  }

}

//*******************************************************************
// SpectrumDAO represents a simple Dao for the access to data
// in data/spectrum.json
//*******************************************************************
class SpectrumDAO{

  constructor(path) {
    let json = require(path)
    this.spectrum = json ? json.spectrum : [] // nel caso sia vuoto?
  }

  getAllSpectrum(){
    return this.spectrum
  }

  getSpectrum(frequencies){
    let spectrum = this.getAllSpectrum()

    if( Array.isArray(spectrum) &&
        Array.isArray(frequencies) &&
        frequencies.length == 2)
    {
      let start = frequencies[0]
      let end = frequencies[1]

      return spectrum.filter(s => s.x >= start && s.x<=end)

    }

  }

  getSpectrumByArea(areaDb, areaId) {

    let area = areaDb.getArea(areaId)

    if(area)
      return spectrumDAO.getSpectrum(area.frequencies)
    else
      return null

  }

}
//*******************************************************************

//*******************************************************************
// GraphQL Server
//*******************************************************************

var areasJsonPath = "../data/areas.json"
var spectrumJsonPath = "../data/spectrum.json"

var areaDAO = new AreaDAO(areasJsonPath)
var spectrumDAO = new SpectrumDAO(spectrumJsonPath)

const resolvers = {
  Query: {
    info: () => `This is the API of Nightingale spectrum`,
    areas: () => areaDAO.getAreas(),
    spectrum: (parent, args) => spectrumDAO.getSpectrumByArea(areaDAO, args.areaId),
    allSpectrum: () => spectrumDAO.getAllSpectrum()
  }
}

const server = new GraphQLServer({
  typeDefs: './schema.graphql',
  resolvers,
})

server.start(() => console.log(`Server is running on http://localhost:4000`))

//*******************************************************************


